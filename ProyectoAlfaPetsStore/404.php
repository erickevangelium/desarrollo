<!DOCTYPE html>
<html>
    <head>
        <title>Pets Store</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="css/index.css" type="text/css"/>
        <link rel="stylesheet" href="css/header.css" type="text/css"/>
        <script type="text/javascript" src="script/jquery.js"></script>
        <script type="text/javascript" src="script/index.js"></script>
    </head>
    <body>
        <?php
            include('include/header.php');
        ?>
        <div id="superior" class="contenedor l">
            <div id="logo" class="l">
                <img src="media/imagenes/logo.png" width="40%">
            </div>
            <div id="buscador" class="l">
                <input type="text" name="">
            </div>
        </div>
        <div id="categorias" class="contenedor l">
            <nav class="menu l">
                <ul>
                    
                </ul>
            </nav>
            <div id="ofertas"><div class="slider l">
                    <ul>
                        <li>
                            <img src="media/imagenes/promociones/01.jpg">
                        </li>
                        <li>
                            <img src="media/imagenes/promociones/02.jpg">
                        </li>
                        <li>
                            <img src="media/imagenes/promociones/03.jpg">
                        </li>
                        <li>
                            <img src="media/imagenes/promociones/04.jpg">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="productos" class="contenedor l">
            <div class="contenedor75 l">
                <div class="tituloContenedor">Nada por el momento</div>
                <div class="produContenedor">
                	<img src="media/imagenes/reptiles2.png" width="100%">
                    </div>
            </div>
            <div class="contenedor25 l">
                <div class="contenedorsup"><div class="tituloContenedor">Mi carrito</div>
                    <p>No tienes productos en tu carrito de compras</p></div>
                <div class="contenedormed"><div class="tituloContenedor">Comentarios</div></div>
                <div class="contenedorinf"><div class="tituloContenedor">Registro</div></div>
            </div>
        </div>
    </body>
</html>