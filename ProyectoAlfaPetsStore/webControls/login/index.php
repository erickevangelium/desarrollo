
<!DOCTYPE html>
<html lang="en" class="no-js">

    <head>

        <meta charset="utf-8">
        <title>Inicio de sesión PetsStore</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
        <link rel="stylesheet" href="assets/css/reset.css">
        <link rel="stylesheet" href="assets/css/supersized.css">
        <link rel="stylesheet" href="assets/css/style.css">

    </head>

    <body>

        <div class="page-container">
            <h1>Inicio de Sesión</h1>
            <form action="../Clientes/setSession.php" method="post">
                <input  id="Correo" name="Correo" type="text" class="username" placeholder="Nombre de usuario">
                <input id="password" type="password" name="password" class="password" placeholder="Contraseña">
                <button type="submit">Acceder</button>
                <button onclick="regresar()">Cancelar</button>


        <p class="form_link"> <h2>Si eres nuevo</h2> <a href="../Clientes/FormularioClientes.php">ingresa aquí</a></p>

                <div class="error"><span>+</span></div>
            </form>
        </div>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.8.2.min.js"></script>
        <script src="assets/js/supersized.3.2.7.min.js"></script>
        <script src="assets/js/supersized-init.js"></script>
        <script src="assets/js/scripts.js"></script>
         <script src="../Clientes/ValidarClientes.js"></script>


    </body>

</html>

