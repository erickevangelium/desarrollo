<!DOCTYPE html>
<html>
<head>
	<title>Consultar</title>
  <link rel="stylesheet" type="text/css" href="Tabla.css">
	<link rel="stylesheet" type="text/css" href="Proveedores.css">
	<script>
  function fnEliminar(id_proveedor){
    var r = confirm("¿Desea eliminar el proveedor?");
    if (r == true) {
      location.href="Eliminar_Proveedores.php?id_proveedor=" + id_proveedor;
    }
  }
        function cancel() {
            location.href = "../Clientes/admin.php";
        }
  function agregarProveedor() {
            location.href = "FormularioProveedores.php";
        }
  </script>
</head>
<body>
 <div id='main-container'>
 	<h1>Consulta de  Proveedores</h1>
    <table>
        <thead>
            <tr>
               <th>Proveedor</th>
               <th>Contacto</th>
               <th>Teléfono</th> 
               <th>Correo</th>
               <th>Estado</th>
               <th>Municipio</th>
               <th>Colonia</th>
               <th>CP</th>
               <th>Calle</th>
               <th>Manzana</th>
               <th>Lote</th>
               <th>Eliminar</th>
               <th>Modificar</th>
            </tr>
        </thead>
    <?php
    include('../headcon.php');
    $sql = "SELECT *  FROM proveedores inner join estado on estado.id_estado=proveedores.id_estado  inner join municipio on municipio.id_municipio=proveedores.id_municipio inner join colonia on colonia.id_colonia=proveedores.id_colonia inner join codigo_postal on codigo_postal.id_cp=proveedores.id_cp;" ;
    //echo $sql;
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $row["proveedor"]. "</td>";
        echo "<td>" . $row["contacto"]. "</td>";   
        echo "<td>" . $row["tel"]. "</td>";         
        echo "<td>" . $row["correo"]. "</td>";
        echo "<td>" . $row["estado"]. "</td>";
        echo "<td>" . $row["municipio"]. "</td>";
        echo "<td>" . $row["colonia"]. "</td>";
        echo "<td>" . $row["cp"]. "</td>";
        echo "<td>" . $row["calle"]. "</td>";
        echo "<td>" . $row["no_ext"]. "</td>";
        echo "<td>" . $row["no_int"]. "</td>";
        echo "<td><a href='#' onclick='fnEliminar(" . $row["id_proveedor"] . ");'><img src='../../media/imagenes/eliminar.png' width='30px'/></a></td>";
echo "<td><a href='FormuModificar_Proveedores.php?id_proveedor=".$row["id_proveedor"]."'><img src='../../media/imagenes/edit.png' width='30px'/></a></td>";
        echo "</tr>";
      }
    }
    ?>
  </table>
    <input type="button" name="" value="Cancelar" class="btn_enviar" onclick="cancel();">
    <input type="button" name="" value="Agregar" class="btn_enviar" onclick="agregarProveedor();">

</div>
</body>
</html>
<?php

?>