<!DOCTYPE html>
<html>
<head>
	<title>Registro de Proveedores</title>
	<link rel="stylesheet" type="text/css" href="Proveedores.css">
        <script type="text/javascript" src="jquery.js"></script>
	<script src="ValidarProveedores.js"></script>
    <script>
        function cancel() {
            location.href = "../Clientes/admin.php";
        }
        function consul() {
            location.href = "Consultar_Proveedores.php";
        }</script>
</head>
<body>
<h1>Formulario de Registro de Proveedores</h1>
<form action="Alta_Proveedores.php" method="Post" class="form-register" onsubmit="return validar();">
	<div class="contenedor-inputs">
		<input id="nombreP" type="text" name="nombreP" placeholder="Nombre" class="input-48" required="">
		<input id="contacto" type="text" name="contacto" placeholder="Contacto" class="input-48" required="">
		<input id="telefono" type="text"  name="telefono" placeholder="Teléfono" onkeypress="return Solonumeros(event);" class="input-48" required="">
		<input id="email" type="email" name="email" placeholder="Correo electrónico" class="input-48" required="">
		<div id="estado-div" class="input-48">
		<select id="estado" name="estado" class="input-100" required="" onchange="seleccioneEstado()">
			<option value="0">Seleccione un estado</option>
		<?php
			include('../headcon.php');
			$query = "SELECT * from estado";
			$resultado=$conn->query($query);
			while($row = $resultado->fetch_assoc()){

		?>
			<option value="<?php echo $row['id_estado'];?>"><?php echo $row['estado'];?> </option>
			<?php }
?>
	</select></div><div id="municipio-div" class="input-48">
		<select id="municipio" name="municipio" class="input-100" required="">
			<option value="0">Seleccione un municipio</option></select></div><div id="colonia-div" class="input-48">
		<select id="colonia" name="colonia" class="input-100" required="">
			<option value="0">Seleccione una colonia</option></select></div><div id="cp-div" class="input-48">
		<select id="cp" name="cp" class="input-100" required="">
			<option value="0">Seleccione un código postal</option></select></div>
		<input id="calle" type="text" name="calle" placeholder="Calle" class="input-48" required="">
		<input id="numeroI" type="text" name="numeroI" placeholder="Número interior" class="input-48" required="">
		<input id="numeroE" type="text" name="numeroE" placeholder="Número exterior" class="input-48" required="">
		<input type="submit" name="" value="Registrar" disabled="disabled" class="btn_enviar blanco">
		<input type="submit" name="" value="Registrar" class="btn_enviar">
		<input type="button" name="" value="Cancelar" class="btn_enviar" onclick="cancel();">

	</div>
</form>
</body>
</html>