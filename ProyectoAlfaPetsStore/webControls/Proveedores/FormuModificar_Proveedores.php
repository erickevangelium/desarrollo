<!DOCTYPE html>
<html>
<head>
	<title>Modificar</title>
	<link rel="stylesheet" type="text/css" href="Proveedores.css">
	<script src="ValidarProveedores.js"></script>
	<script>
		function back3() {
            location.href = "Consultar_Proveedores.php";
        }
	</script>
</head>
<body>
	<h1>Formulario de Registro</h1>
	<?php

	include('../headcon.php');
	$id_proveedor= $_GET['id_proveedor'];

	$query = "SELECT * from proveedores where id_proveedor = " . $id_proveedor;
	$resultado=$conn->query($query);
	while($row = $resultado->fetch_assoc()){
		?>
<form action="Modificar_Proveedores.php?id_proveedor=<?php echo $id_proveedor;?>" method="Post" class="form-register" onsubmit="return validar();">
	<h2 class="form-title">Actualiza los datos del proveedor</h2>
	<div class="contenedor-inputs">
		<input id="nombreP" type="text" name="nombreP" placeholder="Nombre" class="input-100" required="" value="<?php echo $row['proveedor'];?>">
		<input id="contacto" type="text" name="contacto" placeholder="Nombre de contacto" class="input-100" required="" value="<?php echo $row['contacto'];?>">
		<input id="telefono" type="text"  name="telefono" value="<?php echo $row['tel'];?>" placeholder="Teléfono"  onkeypress="return Solonumeros(event);" class="input-48" required="">
		<input id="email" type="email" name="email" placeholder="Correo electrónico" class="input-48" required="" value="<?php echo $row['correo'];?>">
		<input id="estado" type="text" name="estado" placeholder="Estado" class="input-48" required="" value="<?php echo $row['id_estado'];?>" >
		<input id="municipio" type="text" name="municipio" placeholder="Municipio" class="input-48" required="" value="<?php echo $row['id_municipio'];?>">
		<input id="colonia" type="tel"  name="colonia" placeholder="Colonia" class="input-48"  required="" value="<?php echo $row['id_colonia'];?>">
		<input id="cp" type="text" name="cp" placeholder="Código postalL" class="input-48" required="" value="<?php echo $row['id_cp'];?>">
		<input id="calle" type="text" name="calle" placeholder="Calle" class="input-48" required="" value="<?php echo $row['calle'];?>">
		<input id="numeroI" type="text" name="numeroI" placeholder="Número interior" class="input-48" required="" value="<?php echo $row['no_int'];?>">
		<input id="numeroE" type="text" name="numeroE" placeholder="Número exterior" class="input-48" required="" value="<?php echo $row['no_ext'];?>">
		<input type="submit" name="" value="Guardar" class="btn_enviar">
	</div>
</form>
	<div class="contenedor-inputs form-register">
		<input type="submit" name="" value="Regresar" class="btn_enviar" onclick="back3()">
</div>
<?php
}
?>
</body>

</html>

</body>
</html>
<?php

 ?>