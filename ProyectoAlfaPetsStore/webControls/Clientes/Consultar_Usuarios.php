<!DOCTYPE html>
<html>
<head>
	<title>Consultar</title>
  <link rel="stylesheet" type="text/css" href="Tabla.css">
  <link rel="stylesheet" type="text/css" href="Clientes.css">
	<script>
  function fnEliminar(id_cliente){
    var r = confirm("¿Desea eliminar el usuario?");
    if (r == true) {
      location.href="Eliminar_Usuarios.php?id_cliente=" + id_cliente;
    }
  }
  function back(){
      location.href="admin.php";
   
  }
  </script>
</head>
<body>
 <div id='main-container'>
 	<h1>Usuarios</h1>
    <table>
        <thead>
            <tr>
               <th>Usuario</th>
               <th>Nombre</th>
               <th>Correo Electrónico</th>
               <th>Teléfono</th> 
               <th>Estado</th>
               <th>Eliminar</th>
               <th>Modificar</th>
            </tr>
        </thead>
    <?php
    include('../headcon.php');
    $sql = "SELECT *  FROM Usuario inner join clientes on usuario.id_cliente=clientes.id_cliente  WHERE NOT tipo ='Master'" ;
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $row["id_cliente"]. "</td>";
        echo "<td>" . $row["cliente"]. "</td>";   
        echo "<td>" . $row["correo"]. "</td>";
        echo "<td>" . $row["tel"]. "</td>";      
        echo "<td>" . $row["estado"]. "</td>";
        echo "<td><a href='#' onclick='fnEliminar(" . $row["id_cliente"] . ");'><img src='../../media/imagenes/eliminar.png' width='30px'/></a></td>";

echo "<td><a href='FormuModificar_Usuarios.php?id_cliente=".$row["id_cliente"]."'><img src='../../media/imagenes/edit.png' width='30px'/></a></td>";
        echo "</tr>";
      }
    }
    ?>
  </table>
        <input type="button" name="" value="Regresar" class="btn_enviar" onclick="back();">
</div>
</body>
</html>
<?php


?>