<!DOCTYPE html>
<html>
<head>
	<title>Registro</title>
	<link rel="stylesheet" type="text/css" href="Clientes.css">
        <script type="text/javascript" src="jquery.js"></script>
	<script src="ValidarClientes.js"></script>
    <script>
        function regresar() {
            location.href = "../../";
        }</script>
</head>
<body>
<form action="Alta_Clientes.php" method="Post" class="form-register" onsubmit="return validar();">
	<h2 class="form-title">Crea una cuenta</h2>
	<div class="contenedor-inputs">
		<input id="nombre" type="text" name="nombre" placeholder="Nombre" class="input-48" required>
		<input id="email" type="email" name="email" placeholder="Correo Electrónico" class="input-48" required>
		<input id="password" type="password" name="password" placeholder="Contraseña" class="input-48" required>
		<input id="password-confirm" type="password" name="password-confirm" placeholder="Confirma contraseña" class="input-48" required>
		<input id="telefono" type="text"  name="telefono" placeholder="Teléfono" onkeypress="return Solonumeros(event);" class="input-48" required>

		<div id="estado-div" class="input-48">
		<select id="estado" name="estado" class="input-100" required onchange="seleccioneEstado()">
			<option value="0">Seleccione un estado</option>
		<?php
			include('../headcon.php');
			$query = "SELECT * from estado";
			$resultado=$conn->query($query);
			while($row = $resultado->fetch_assoc()){

		?>
			<option value="<?php echo $row['id_estado'];?>"><?php echo $row['estado'];?> </option>
			<?php }
?>
	</select></div><div id="municipio-div" class="input-48">
		<select id="municipio" name="municipio" class="input-100" required>
			<option value="0">Seleccione un municipio</option></select></div><div id="colonia-div" class="input-48">
		<select id="colonia" name="colonia" class="input-100" required>
			<option value="0">Seleccione una colonia</option></select></div><div id="cp-div" class="input-48">
		<select id="cp" name="cp" class="input-100" required>
			<option value="0">Seleccione un código postal</option></select></div>
		<input id="calle" type="text" name="calle" placeholder="CALLE" class="input-48" required>
		<input id="numeroI" type="text" name="numeroI" placeholder="Número interior" class="input-48" required>
		<input id="numeroE" type="text" name="numeroE" placeholder="Número exterior" class="input-48" required>
		<input type="submit" name="" value="Registrar" class="btn_enviar">
        <input type="button" name="" value="Cancelar" class="btn_enviar" onclick="regresar();">
        <p class="form_link"> ¿Ya tienes una cuenta? <a href="../login/index.php">ingresa aquí</a></p>
	</div>
</form>
</body>
</html>