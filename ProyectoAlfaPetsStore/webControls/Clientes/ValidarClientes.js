function validar() {
	var nombre, email, telefono, estado, municipio, colonia, cp, calle, numI, numE, expresion;
	nombre = document.getElementById("nombre").value;
	email = document.getElementById("email").value;
	telefono = document.getElementById("telefono").value;
	estado = document.getElementById("estado").value;
	municipio = document.getElementById("municipio").value;
	colonia = document.getElementById("colonia").value;
	cp = document.getElementById("cp").value;
	calle = document.getElementById("calle").value;
	numI = document.getElementById("numeroI").value;
	numE = document.getElementById("numeroE").value;



	if (nombre === "" || email === "" || telefono === "" || estado === "" || municipio === "" || colonia === "" || cp === "" || numI === "" || numE === "") {
		alert("Todos los campos son obligatorios");
		return false;
	} else if (nombre.length > 30) {
		alert("El nombre es muy largo");
		return false;
	} else if (nombre.length < 3) {
		alert("El nombre es muy corto");
		return false;
	} else if (email.length > 75) {
		alert("El nombre es muy largo");
		return false;
	} else if (telefono.length > 10) {
		alert("El telefono es muy largo");
		return false;
	} else if (telefono.length < 8) {
		alert("El telefono es muy corto");
		return false;
	}

}

function Solonumeros(e) {
	key = e.keyCode || e.which;
	teclado = String.fromCharCode(key);
	numero = "0123456789";
	especiales = "8-37 -38-46";
	teclado_especial = false;

	for (var i in especiales) {
		if (key == especiales[i]) {
			teclado_especial = true;
		}
	}
	if (numero.indexOf(teclado) == -1 && !teclado_especial) {
		return false;
	}
}

function seleccioneEstado() {
	var id2 = $("#estado").val();
	$.post("municipio.php", {
			id: id2
		},
		function(data, status) {
			$("#municipio-div").html(data);
			seleccioneMunicipio()
		});
}

function seleccioneMunicipio() {
	var id2 = $("#municipio").val();
	$.post("colonia.php", {
			id: id2
		},
		function(data, status) {
			$("#colonia-div").html(data);
			seleccioneColonia();
		});

}

function seleccioneColonia() {
	var id2 = $("#colonia").val();
	$.post("cp.php", {
			id: id2
		},
		function(data, status) {
			$("#cp-div").html(data);

		});
}

function exit() {
	location.href = "sessionClose.php";
}

function regresar() {
	location.href = "../../";
}
function regresar2() {
	location.href = "Base.php";
}

function Clientes() {
	location.href = "Consultar_Clientes.php"
}

function Proveedores() {
	location.href = "../Proveedores/Consultar_Proveedores.php"
}

function Productos() {
	location.href = "../Productos/"
}

function Categorias() {
	location.href = "../Categorias/Categorias.php"
}

function Usuarios() {
	location.href = "Consultar_Usuarios.php"
}

function perfil() {
	location.href = "ComprasTotales.php"
}

function perfilUser(id) {
	location.href = "FormuModificar_Clientes2.php?id="+id;
}

function comprasUser(id) {
	location.href = "consultarCompras.php?id="+id;
}

function galerias() {
	location.href ="galeria.php"
}