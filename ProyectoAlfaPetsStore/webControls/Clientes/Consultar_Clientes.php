<!DOCTYPE html>
<html>
<head>
	<title>Consultar</title>
  <link rel="stylesheet" type="text/css" href="Tabla.css">
  <link rel="stylesheet" type="text/css" href="Clientes.css">
	<script>
  function fnEliminar(id_cliente){
    var r = confirm("¿Desea eliminar el usuario?");
    if (r == true) {
      location.href="Eliminar_Clientes.php?id_cliente=" + id_cliente;
    }
  }
  function back(){
      location.href="admin.php";
   
  }
  </script>
</head>
<body>
 <div id='main-container'>
 	<h1>Clientes</h1>
    <table>
        <thead>
            <tr>
               <th>Id_Clientes</th>
               <th>Nombre</th>
               <th>Correo Electrónico</th>
               <th>Teléfono</th> 
               <th>Estado</th>
               <th>Municipio</th>
               <th>Colonia</th>
               <th>CP</th>
               <th>Calle</th>
               <th>Manzana</th>
               <th>Lote</th>
               <th>Eliminar</th>
               <th>Modificar</th>
            </tr>
        </thead>
    <?php
    include('../headcon.php');
    $sql = "SELECT *  FROM clientes inner join estado on estado.id_estado=clientes.id_estado  inner join municipio on municipio.id_municipio=clientes.id_municipio inner join colonia on colonia.id_colonia=clientes.id_colonia inner join codigo_postal on codigo_postal.id_cp=clientes.id_cp;" ;
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $row["id_cliente"]. "</td>";
        echo "<td>" . $row["cliente"]. "</td>";   
        echo "<td>" . $row["correo"]. "</td>";
        echo "<td>" . $row["tel"]. "</td>";      
        echo "<td>" . $row["estado"]. "</td>";
        echo "<td>" . $row["municipio"]. "</td>";
        echo "<td>" . $row["colonia"]. "</td>";
        echo "<td>" . $row["cp"]. "</td>";
        echo "<td>" . $row["calle"]. "</td>";
        echo "<td>" . $row["no_ext"]. "</td>";
        echo "<td>" . $row["no_int"]. "</td>";
        echo "<td><a href='#' onclick='fnEliminar(" . $row["id_cliente"] . ");'><img src='../../media/imagenes/eliminar.png' width='30px'/></a></td>";

echo "<td><a href='FormuModificar_Clientes.php?id_cliente=".$row["id_cliente"]."'><img src='../../media/imagenes/edit.png' width='30px'/></a></td>";
        echo "</tr>";
      }
    }
    ?>
  </table>
        <input type="button" name="" value="Regresar" class="btn_enviar" onclick="back();">
</div>
</body>
</html>
<?php


?>