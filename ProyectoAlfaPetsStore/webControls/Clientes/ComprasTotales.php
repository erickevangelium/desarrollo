<!DOCTYPE html>
<html>
<head>
	<title>Consultar</title>
  <link rel="stylesheet" type="text/css" href="Tabla.css">
  <!--<link rel="stylesheet" type="text/css" href="Clientes.css">-->
  <link rel="stylesheet" type="text/css" href="Ventas.css">

	<script>
  function back(){
      location.href="admin.php";
   
  }
  function mostrarPDF(id){
    window.open("../../pdf/index.php?id="+id, "PDF");

  }
  </script>
</head>
<body>
 <div id='main-container'>
 	<h1>Clientes</h1>
    <table>
        <thead>
            <tr>
               <th>Folio</th>
               <th>Fecha</th>
               <th>Monto</th>
               <th>Cliente</th>
               <th>Ver</th> 
            </tr>
        </thead>
    <?php
    include('../headcon.php');
    $sql = "SELECT * FROM venta inner join clientes on venta.id_cliente=clientes.id_cliente order by fecha desc";
    //echo $sql;
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $row["folio"]. "</td>";
        echo "<td>" . $row["fecha"]. "</td>";   
        echo "<td>" . $row["monto"]. "</td>";
        echo "<td>" . $row["cliente"]. "</td>";
        echo "<td><a href='#' onclick='mostrarPDF(" . $row["folio"] . ");'><img src='../../media/imagenes/ver.png' width='30px'/></a></td>";
        echo "</tr>";
      }
    }
    ?>
  </table>
        <input type="button" name="" value="Regresar" class="btn_enviar" onclick="back();">
</div>
</body>
</html>
<?php


?>