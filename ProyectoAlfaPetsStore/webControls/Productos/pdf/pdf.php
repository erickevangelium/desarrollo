<?php
	require 'fpdf/fpdf.php';

	class PDF extends FPDF
	{
		
		function Header()
		{
			$this->Image('../media/imagenes/perro.png', 10, 10, 40);
			$this->SetFont('Arial', '', 6);
			$this->Cell(40);
			$this->MultiCell(40,3,utf8_decode("Domicilio  local:\nUniversidad Tecnologia de Tecamac\n\nCarretera mexico pachuca kilometro 31 (no es el de la pelicula)"),0,'J',0);
			$this->setXY(90,10);
			$this->MultiCell(40,3,utf8_decode("Dudas:\nNo marcar al 55-FREDO-123\n\nDudas o aclaraciones\nfredo@gmail.com"),0,'J',0);
			$this->setXY(130,10);
			$this->Cell(35, 5, "Factura PestStore", 1, 0, 'C');
			$this->Cell(35, 5, "Factura PestStore", 1, 0, 'C');
			$this->setXY(130,15);
			$this->Cell(35, 15, "Factura PestStore", 1, 0, 'C');
			$this->Cell(35, 15, "Factura PestStore", 1, 0, 'C');
			$this->Ln(22);
		}
		function Footer()
		{
			$this->SetY(-15);
			$this->SetFont('Arial', 'I', 8);
			$this->Cell(190, 10, 'Pagina '.$this->PageNo().'/{nb}', 0, 0, 'R');
		}
	}
	
?>