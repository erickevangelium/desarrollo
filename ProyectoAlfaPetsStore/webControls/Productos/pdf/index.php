<?php
	include 'pdf.php';
	$pdf=new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage();

	$pdf->SetFillColor(218,218,218);
	$pdf->SetFont('Arial','',8);
	$pdf->MultiCell(190,5,utf8_decode("Cliente:\nDireccion:\nTeléfono:\nCorreo:"),1,'J',1);
	$pdf->Ln(2);
	include('../webControls/headcon.php');
    $sql = "SELECT *  FROM Venta inner join detalle_venta on venta.folio=detalle_venta.folio inner join productos on productos.id_producto=detalle_venta.id_produco where folio=".$id;
    //echo $sql
    $Y=64;
    $tama=0;
    $tamaT=0;
    $totalCantidad=0;
    $totalDinero=0;
    $result = $conn->query($sql);

	$pdf->setXY(10,59);
	$pdf->Cell(8,5,utf8_decode("N°"),1,0,'C',0);
	$pdf->Cell(32,5,utf8_decode("Producto"),1,0,'C',0);
	$pdf->Cell(90,5,utf8_decode("Descripción"),1,0,'C',0);
	$pdf->Cell(20,5,utf8_decode("Cantidad"),1,0,'C',0);
	$pdf->Cell(20,5,utf8_decode("Costo"),1,0,'C',0);
	$pdf->Cell(20,5,utf8_decode("Subtotal"),1,1,'C',0);
    if ($result->num_rows > 0) {
      // output data of each row
      $numero=0;
      while($row = $result->fetch_assoc()) {
      	if($tamaT>=150) {
		$pdf->AddPage();
		$tamaT=0;
		$Y=42;
	$pdf->Cell(8,5,utf8_decode("N°"),1,0,'C',0);
	$pdf->Cell(32,5,utf8_decode("Producto"),1,0,'C',0);
	$pdf->Cell(90,5,utf8_decode("Descripción"),1,0,'C',0);
	$pdf->Cell(20,5,utf8_decode("Cantidad"),1,0,'C',0);
	$pdf->Cell(20,5,utf8_decode("Costo"),1,0,'C',0);
	$pdf->Cell(20,5,utf8_decode("Subtotal"),1,1,'C',0);
		
	}
      	$numero++;
      	$tama=5;
      	$tama+=$tama*floor(strlen($row["descripcion"])/56);
      	$tamaT+=$tama;	
    $pdf->setXY(10,$Y);
	$pdf->Cell(8,$tama,utf8_decode($numero),1,0,'C',0);
	$pdf->Cell(32,$tama,utf8_decode($row["producto"]),1,0,'C',0);
	if($tama==5){
	$pdf->MultiCell(90,$tama,utf8_decode($row["descripcion"]),1,'J',0);
}else{
	$pdf->MultiCell(90,5,utf8_decode($row["descripcion"]),1,'J',0);

}
	$pdf->setXY(140,$Y);
	$pdf->Cell(20,$tama,utf8_decode($row["cantidad"]),1,0,'C',0);
	$pdf->Cell(20,$tama,utf8_decode($row["precio_venta"]),1,0,'C',0);
	$pdf->Cell(20,$tama,utf8_decode($row["cantidad"]*$row["precio_venta"]),1,1,'C',0);
	$totalCantidad+=$row["cantidad"];
	$totalDinero+=($row["cantidad"]*$row["precio_venta"]);
	$Y+=$tama;
	$tamaT+=$tama;

	
		}
	}
	$pdf->setXY(120,$Y);
	$pdf->Cell(20,$tama,utf8_decode("Productos:"),1,0,'C',0);
	$pdf->Cell(20,$tama,$totalCantidad,1,0,'C',0);
	$pdf->Cell(20,$tama,utf8_decode("Total"),1,0,'C',0);
	$pdf->Cell(20,$tama,$totalDinero,1,1,'C',0);
	$pdf->Output();

?>