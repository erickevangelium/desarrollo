<?php
	session_start();
	include 'webcontrols/headcon.php';
	if(isset($_SESSION['carrito'])){
		if(isset($_GET['id'])){
			$arreglo=$_SESSION['carrito'];
			$encontro=false;
			$numero=0;
			for($i=0;$i<count($arreglo);$i++){
				if($arreglo[$i]['Id']==$_GET['id']){
					$encontro=true;
					$numero=$i;
				}
			}
			if($encontro==true){
				$arreglo[$numero]['Cantidad']=$arreglo[$numero]['Cantidad']+1;
				$_SESSION['carrito']=$arreglo;
			}else{
				$nombre="";
				$precio=0;
				$imagen="";
				$sql = "select * from productos where id_producto=".$_GET['id'];
				$re= $conn->query($sql);
				while ($f=$re->fetch_assoc()) {
					$nombre=$f['producto'];
					$precio=$f['precio_venta'];
					$imagen=$f['imagen'];
				}
				$datosNuevos=array('Id'=>$_GET['id'],
								'Nombre'=>$nombre,
								'Precio'=>$precio,
								'Imagen'=>$imagen,
								'Cantidad'=>1);
				array_push($arreglo, $datosNuevos);

						$_SESSION['carrito']=$arreglo;
					}}
	}else{
		if(isset($_GET['id'])){
			$nombre="";
			$precio=0;
			$imagen="";
			$sql = "select * from productos where id_producto=".$_GET['id'];
			$re= $conn->query($sql);
			while ($f=$re->fetch_assoc()) {
				$nombre=$f['producto'];
				$precio=$f['precio_venta'];
				$imagen=$f['imagen'];
			}
			$arreglo[]=array('Id'=>$_GET['id'],
							'Nombre'=>$nombre,
							'Precio'=>$precio,
							'Imagen'=>$imagen,
							'Cantidad'=>1);
			$_SESSION['carrito']=$arreglo;
		}
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8"/>
	<title>Carrito de Compras</title>
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript"  src="js/scripts.js"></script>
</head>
<body>
	<header>
		<img src="../../media/imagenes/logo.png" id="logo">
		
	</header>
	<section>
		<?php
			$total=0;
			if(isset($_SESSION['carrito'])){
			$datos=$_SESSION['carrito'];
			$total=0;
			for($i=0;$i<count($datos);$i++){
	?>
				<div class="producto">
					<center>
						<img src="files/<?php echo $datos[$i]['Imagen'];?>"><br>
						<span ><?php echo $datos[$i]['Nombre'];?></span><br>
						<span>Precio: <?php echo $datos[$i]['Precio'];?></span><br>
						<span>Cantidad: 
							<input type="text" value="<?php echo $datos[$i]['Cantidad'];?>"
							data-precio="<?php echo $datos[$i]['Precio'];?>"
							data-id="<?php echo $datos[$i]['Id'];?>"
							class="cantidad">
						</span><br>
						<span class="subtotal">Subtotal:<?php echo $datos[$i]['Cantidad']*$datos[$i]['Precio'];?></span><br>
						<a href="#" class="eliminar" data-id="<?php echo $datos[$i]['Id']?>">Eliminar</a>
					</center>
				</div>
			<?php
				$total=($datos[$i]['Cantidad']*$datos[$i]['Precio'])+$total;
			}
			}else{
				echo '<center><h2>No has añadido ningun producto</h2></center>';
			}
			echo '<center><h2 id="total">Total: '.$total.'</h2></center>';
			if($total!=0){
			?>
				<center><input type="submit" value="comprar" class="aceptar" style="width:200px" onclick="comprartodo(<?php echo $_SESSION["idUser"]; ?>)"></center>
			<?php
			}
		?>
		<center><a href="../../">Ver catálogo</a></center>
	</section>
</body>
</html>