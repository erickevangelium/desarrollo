<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
        <link rel="stylesheet" type="text/css" href="css/index.css">
        <link rel="stylesheet" type="text/css" href="css/Clientes.css">
		<title>Registro</title>
		<script>
  function back(){
      location.href="../Clientes/admin.php";
  }
  </script>
	</head>
	<body>
		<div id="container" class="containeramod completo">
			<form method="post" id="formA" action="webcontrols/registro.php" enctype="multipart/form-data" class="ocultar">
				<table>
					<tr>
						<td>Producto:</td><td><input class="inputs" type="text" name="txtproducto" id="txtproducto" required="required" /></td>
					</tr>
					<tr>
						<td>Precio compra:</td><td><input class="inputs" type="text" name="txtpreciocompra" id="txtpreciocompra" required="required" /></td>
					</tr>
					<tr>
						<td>Precio venta:</td><td><input class="inputs" type="text" name="txtprecioventa" id="txtprecioventa" required="required" /></td>
					</tr>
					<tr>
						<td>Descripción:</td><td><input class="inputs" type="text" name="txtdescripcion" id="txtdescripcion" required="required" /></td>
					</tr>
					<tr>
						<td>Imagen:</td>
						<td><input class="botonImagen" type="file" name="archivo" id="archivo" required="required" size="35" accept="image/png, .jpeg, .jpg, image/gif" ><input type="hidden" name="action" value="upload">
						</td>
					</tr>
					<tr>
						<td>Categoría:</td><td>
				<select id="txtidcategoria" name="txtidcategoria">
			<option value="0">Seleccione una categoría</option>
		<?php
			include('webcontrols/conexion.php');
			$sql = "SELECT * from categoria";
$mysqli -> real_query($sql);
$query = $mysqli->store_result();
		while($row = $query ->fetch_assoc()){
		?>
			<option value="<?php echo $row['id_categoria'];?>"><?php echo $row['categoria'];?> </option>
			<?php }
?>
						</td>
					</tr>
					<tr>
						<td>Proveedor:</td><td>
				<select id="txtidprovedor" name="txtidprovedor">
			<option value="0">Seleccione un proveedor</option>
		<?php
			include('webcontrols/conexion.php');
			$sql = "SELECT * from proveedores";
$mysqli -> real_query($sql);
$query = $mysqli->store_result();
		while($row = $query ->fetch_assoc()){
		?>
			<option value="<?php echo $row['id_proveedor'];?>"><?php echo $row['proveedor'];?> </option>
			<?php }
?>
						</td>
					</tr>
				</table>
				<input type="submit" value="Guardar" class="boton"/>
			</form>
			<input type="submit" id="cancelA" value="Cancelar" class="boton ocultar" onclick="CAgregar()"/>
			<?php	
				include 'webcontrols/consulta.php';
	        ?>
		</div>
	</body>
		<script type="text/javascript" src="js/index.js"></script>
		<script type="text/javascript" src="js/jquery.js"></script>
</html>