var inicio = function() {
	$(".cantidad").keyup(function(e) {
		if ($(this).val() != '') {
			if (e.keyCode == 13) {
				var id = $(this).attr('data-id');
				var precio = $(this).attr('data-precio');
				var cantidad = $(this).val();
				if (cantidad == 0) {
					$(this).parentsUntil('.producto').remove();
					$.post('eliminar.php', {
						Id: id
					}, function(a) {

				location.href = "AgregarCarrito.php";
					});
				} else {
					$(this).parentsUntil('.producto').find('.subtotal').text('Subtotal: ' + (precio * cantidad));
					$.post('modificarDatos.php', {
						Id: id,
						Precio: precio,
						Cantidad: cantidad
					}, function(e) {
						$("#total").text('Total: ' + e);
					});
				}
			}
		}
	});
	$(".eliminar").click(function(e) {
		e.preventDefault();
		var id = $(this).attr('data-id');
		$(this).parentsUntil('.producto').remove();
		$.post('eliminar.php', {
			Id: id
		}, function(a) {

			if (a == '0') {
				location.href = "AgregarCarrito.php";
			}
		});

	});
}
$(document).on('ready', inicio);
function comprartodo(id){
	$.post('comprar.php', {
			id: id
		}, function(a) {	
			if (a=="Error") {

			}else{
			enviarPDF(a);
			}
		});
}
function enviarPDF(id){
	$.post('../../pdf/enviar.php', {
		id:id
		}, function(a) {
			alert("Correo enviado a: "+a);
				location.href = "../../";
		});
}