<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/Clientes.css">
	<meta charset="utf-8">
	<title>Registrar Categoría</title>
    <script>
        function regresar() {
            location.href = "../Categorias.php";
        }
    </script>
</head>
<body>
<h1>Registrar Categoría</h1>
<form action="AltaCategorias.php" method="Post" class="form-register" enctype="multipart/form-data">
<h2 class="form-title">Nueva Categoría</h2>
	<div class="contenedor-inputs">
		<input id="nombre" type="text" name="nombre" placeholder="NOMBRE CATEGORÍA" class="input-100" required="">
		<input name="archivo" type="file" size="1024" required /><br/><br/>
		<input type="submit" name="" value="Registrar" class="btn_enviar">
        <input type="button" name="" value="Cancelar" class="btn_enviar" onclick="regresar();">
	</div>
</form>
</body>
</html>