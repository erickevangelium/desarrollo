<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Modificar Categoría</title>
  <link rel="stylesheet" type="text/css" href="../css/Clientes.css">
    <script>
        function regresar() {
            location.href = "../Categorias.php";
        }
    </script>
</head>
<body>
  <h1>Modificar Categoría</h1>
<?php
include('../webcontrols/headcon.php');
$id= $_GET['id'];
$sql = "SELECT categoria, imagen FROM categoria WHERE id_categoria =" . $id;
$result = $conn->query($sql);
while($row = $result->fetch_assoc()) {
?>
<form action="ModificarCategorias.php?id=<?php echo $id; ?>" method="Post" class="form-register" enctype="multipart/form-data">
<h2 class="form-title">Actualizar Categoría</h2>
  <div class="contenedor-inputs">
    <input id="nombre" type="text" name="nombre" placeholder="NOMBRE CATEGORÍA" class="input-100" required="" value="<?php echo $row['categoria'];?>">
    <input name="archivo" type="file" size="1024" required /><br/><br/>
    <input type="submit" name="" value="Modificar" class="btn_enviar">
    <input type="button" name="" value="Cancelar" class="btn_enviar" onclick="regresar();">
  </div>
</form>
<?php
}
?>
</body>
</html>
