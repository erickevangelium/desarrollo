<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Categorías</title>
  <link rel="stylesheet" type="text/css" href="css/Tabla.css">
  <link rel="stylesheet" type="text/css" href="css/Clientes.css">
  <script>
  function fnEliminar(id){
    var r = confirm("¿Desea eliminar la categoría?");
    if (r == true) {
      location.href="sub/BajaCategorias.php?id=" + id;
    }
  }
  function back(){
      location.href="../Clientes/admin.php";
   
  }
  </script>
</head>
<body>
<div id='main-container'>
	<table id="ta">
		<tr>
			<td colspan="1"></td>
			<td colspan="3"><h1>Gestión de Categorías</h1></td>
			<td colspan="1"><a href='#'><a href="sub/FormCategorias.php"><img src='img/add.png' width='30px'/></a></td>
		</tr>
	</table>
 	<table id="tabla">
        <thead>
            <tr>
               <th>Categoría</th>
               <th>Nombre</th>
               <th>Imágen</th>
               <th>Modificar</th>
               <th>Eliminar</th>
            </tr>
        </thead>
    <?php
    include('webcontrols/headcon.php');
    $sql = "SELECT * FROM categoria;" ;
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $row["id_categoria"]. "</td>";
        echo "<td>" . $row["categoria"]. "</td>";
        echo "<td><img src='files/" . $row["imagen"] . "' width='100px' />" . "</td>";
        echo "<td><a href='sub/FormActualizar.php?id=" . $row["id_categoria"] ."'><img src='img/update.png' width='30px'/></a></td>";
        echo "<td><a href='#' onclick='fnEliminar(" . $row["id_categoria"] . ");'><img src='img/delete.png' width='30px'/></a></td>";
        echo "</tr>";
      }
    }
    ?>
  </table>
        <input type="button" name="" value="Regresar" class="btn_enviar" onclick="back();">
</div>
</body>
</html>
<?php
?>