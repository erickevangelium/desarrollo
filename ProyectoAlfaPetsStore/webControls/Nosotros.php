<!DOCTYPE html>
<html>
    <head>
        <title>Nosotros</title>
        <link rel="stylesheet" type="text/css" href="nosotros.css">
    </head>
    <body>
        <div id="body">
            <div id="logo">
                <a href="../index.php"><img src="../media/imagenes/logo.png"></a>
            </div>
            <div id="t1">
                <img id="empresa" src="../media/imagenes/equipo.jpg">
                <h1>Nosotros</h1>
                <p>
                    PetsStore es una empresa en la búsqueda constante de las mejores alternativas  de solución a los problemas y necesidades del sector industrial relacionado a la salud, nutrición y cuidado de los animales, a través de la evolución de los esquemas  de investigación y desarrollo de sus productos  así como en la producción, comercialización y distribución de los mismos. Comprometida con un trato respetuoso, honesto y justo hacia los clientes, proveedores y empleados de la empresa creando así un entorno de confianza  que resulte propicio para los negocios.
                </p>
            </div>
            <div id="t2">
                <h1>Misi&oacute;n</h1>
                <p>
                    PetsStore está comprometida con un trato respetuoso, honesto y justo hacia los clientes, proveedores y empleados de la empresa creando así un entorno de confianza  que resulte propicio para los negocios.
                </p>
            </div>
            <div id="t3">
                <h1>Visi&oacute;n</h1>
                <p>
                    PetsStore aspira a llegar a ser una empresa de clase mundial con capacidad para desarrollar y comercializar productos y servicios de  alta calidad y tecnológicamente avanzados para el mercado de la Salud y Nutrición Animal en todo el mundo, constituyéndose  como la mejor alternativa para satisfacer las  necesidades  del mercado a nivel mundial.
                </p>
            </div>

            <div id="t4">
                <h1>Valores</h1>
                <h2>Integridad - Respeto - Justicia - Honestidad - Libertad</h2>
            </div>
        </div>
        <div>
            <img src="../webControls/Categorias/files/74a3b4_menu_gatito.png" alt="74a3b4_menu_gatito.png">
            <img src="../webControls/Categorias/files/41149f_menu_perrito.png" alt="41149f_menu_perrito.png" width="200px">
        </div>
        <div>
            <div class="footerFinal">
                <?php
                include '../footerGeneral.php';
                ?>    
            </div>
        </div>
    </body>
</html>