 <?php
        session_start();
        $userName = "Iniciar sesión";
        $tipo = "webControls/login/index";
        if (isset($_SESSION["active"])) {
            if($_SESSION["tipo"]=="Master"){
                $tipo="webControls/Clientes/admin";
            }else{
                $tipo="webControls/Clientes/Base";
            }
            $userName = $_SESSION["username"];
        } else {

        }
    ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Pets Store</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="css/index.css" type="text/css"/>
        <link rel="stylesheet" href="css/header.css" type="text/css"/>
        <link rel="stylesheet" href="css/responsive.css" type="text/css"/>
        <script type="text/javascript" src="script/jquery.js"></script>
        <script type="text/javascript" src="script/index.js"></script>
    </head>
    <body>
        <?php
            include('include/header.php');
        ?>
        <div id="superior" class="contenedor l">
            <div id="logo" class="l">
                <img src="media/imagenes/logo.png" width="200px">
            </div>
            <div id="buscador" class="l">
            <input type="text" name="buscadortext" id="buscadortext" placeholder="Ingrese el nombre de lo que quiere buscar" onkeyup="buscala()">
            </div>
        </div>
        <div id="categorias" class="contenedor l">
                    <input type="checkbox" id="btn-menu" />
            <label for="btn-menu">
                            <img src="media/imagenes/Icons/menu.png"></label>
            <nav class="menu l">
                <ul>
                    <?php
                    include('webControls/headcon.php');
                    $sql = "SELECT * FROM categoria;";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        $valor = 1;
                        while ($row = $result->fetch_assoc()) {
                            echo '<li id="categoria'.$row["id_categoria"].'" class="c6 novisible" onclick="busquedaCateoria('.$row["id_categoria"].')"">';
                            echo '<a class="menuNav">';
                            echo "" . $row["categoria"] . "</a>";
                            echo '<img src="webControls/Categorias/files/' . $row["imagen"] . '" alt="' . $row["imagen"] . '">';
                            echo "</li>";
                        }
                    }
                    ?>
                </ul>
            </nav>
            <div id="ofertas"><div class="slider l">
                    <ul>
                        <li>
                            <img src="media/imagenes/promociones/01.png">
                        </li>
                        <li>
                            <img src="media/imagenes/promociones/02.jpg">
                        </li>
                        <li>
                            <img src="media/imagenes/promociones/03.jpg">
                        </li>
                        <li>
                            <img src="media/imagenes/promociones/04.jpg">
                        </li>
                    </ul>
                </div>
                <div class="descuento r">
            <ul>
                <li>
                    <img src="media/imagenes/menu/descuentos.png">
                </li>
            </ul>
        </div>
            </div>
        </div>
        <div id="productos" class="contenedor l">
            <div class="contenedor75 l">
                <div class="tituloContenedor">Productos para tus mascotas</div>
                <div class="produContenedor">
                    <?php
                    include('webControls/headcon.php');
                    $sql = "SELECT *  FROM productos order by RAND() limit 9";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        $o = 1;
                        while ($row = $result->fetch_assoc()) {
                            $image = "webControls/Productos/files/" . $row["imagen"];
                            $imagen = "";
                            if ($o == 2) {
                                $imagen .= '<div class="imageProdu l produmedio">';
                            } else {
                                $imagen .= '<div class="imageProdu l">';
                            }
                            $imagen .= '<img src="' . $image . '"><br/>' .
                                    '<div id="nombre_produ" class"l"><b>' . $row["producto"] . '</b></div>' .
                                    '<div id="desc_produ" class"l">' . $row["descripcion"] . '</div><br/>' .
                                    '<div id="precio_produ" class"l"><label><b>$' . $row["precio_venta"] .
                                    '</b></label><div id="boton_produ" class="r"><input type="submit" value="Ver más"  class="boton l" onclick="mostrarProdu(' . $row["id_producto"] . ')"/></div></div></div>';
                            $o++;
                            if ($o == 4) {
                                $o = 1;
                            }
                            echo $imagen;
                        }
                    }
                    ?></div>
            </div>
            <div class="contenedor25 l">
                <div class="contenedorsup"><div class="tituloContenedor">Carrito de Compras</div>
                    <p> <?php if (isset($_SESSION["active"])) {
                     ?><center>
        <a href="webControls/Productos/AgregarCarrito.php" title="ver carrito de compras" >
            <img src="media/imagenes/carrito.png" width="90px"></center><?php 
        }else{ ?>
                    <center><a href="webControls/login/index.php" title="Registrarse"><img src="media/imagenes/carrito.png" width="90px"></a></center></p><?php } ?></div>
                <div class="contenedormed"><div class="tituloContenedor1">Nosotros</div>
                    <br>
                    <center><a href="webControls/Nosotros.php" title="Nosotros"><img src="media/imagenes/equipo.jpg" width="200px"></a></center>
                </div>
                <div class="contenedorinf"><div class="tituloContenedor">Contáctanos</div>
                    <br>
                    <center><a href="https://www.facebook.com/petsstore" title="Facebook"><img src="media/imagenes/paginas/facebook.png" width="80px"></a></center>
                    <br>
                    <center><a href="https://www.twitter.com/petsstore" title="Twitter"><img src="media/imagenes/paginas/twitter.png" width="80px"></a></center>
                    <br>
                    <center><a href="https://www.instagram.com/petsstore" title="Instagram"><img src="media/imagenes/paginas/1b2ca367caa7eff8b45c09ec09b44c16-instagram-icon-logo-by-vexels.png" width="90px"></a></center>
                </div>
            </div>
        </div>
        <div>
        <div class="footerFinal">
                    <?php
                    include 'footerGeneral.php';
                    ?>    
                </div>
    </div>
    </body>
</html>