-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 09-04-2018 a las 20:15:46
-- Versión del servidor: 5.7.19
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `petsstore`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

DROP TABLE IF EXISTS `alumnos`;
CREATE TABLE IF NOT EXISTS `alumnos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alumno` varchar(30) DEFAULT NULL,
  `curp` varchar(18) DEFAULT NULL,
  `rfc` varchar(13) DEFAULT NULL,
  `telefono` varchar(11) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `imagen` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

DROP TABLE IF EXISTS `categoria`;
CREATE TABLE IF NOT EXISTS `categoria` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `categoria`, `imagen`) VALUES
(3, 'Gatos', '74a3b4_menu_gatito.png'),
(4, 'Perros', '41149f_menu_perrito.png'),
(5, 'Peces', 'a0fbb9_menu_peces.png'),
(6, 'Aves', 'f34e39_menu_pajaros.png'),
(7, 'Reptiles', '462172_menu_reptiles.png'),
(8, 'Pequeños', '1029df_menu_hamsters.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `cliente` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `tel` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `id_estado` int(11) NOT NULL,
  `id_municipio` int(11) NOT NULL,
  `id_colonia` int(11) NOT NULL,
  `id_cp` int(11) NOT NULL,
  `calle` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `no_ext` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `no_int` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_cliente`),
  KEY `id_estado` (`id_estado`),
  KEY `id_municipio` (`id_municipio`),
  KEY `id_colonia` (`id_colonia`),
  KEY `id_cp` (`id_cp`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `cliente`, `tel`, `correo`, `id_estado`, `id_municipio`, `id_colonia`, `id_cp`, `calle`, `no_ext`, `no_int`) VALUES
(1, 'Sergio', '5520147852', 'sergio@gmail.com', 1, 1, 1, 1, 'Ecatepec de Morelos', '1', '30'),
(4, 'Mary', '1212121212', 'mary@gmail.com', 1, 1, 1, 1, 'Amecameca', '12', '12'),
(5, 'Daniel', '5511224477', 'dany@gmail.com', 1, 1, 1, 1, 'Duraznos', '1', '30'),
(6, 'Erick', '1234567895', 'erick@gmail.com', 1, 1, 1, 1, 'Teotihuacan', '58', '85'),
(7, 'Garzon', '5544887799', 'garzon@gmail.com', 1, 1, 1, 1, 'Ecatepec', '12', '50'),
(8, 'Laura', '5533961257', 'laura@gmail.com', 1, 1, 1, 1, 'Flor', '38', '86');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codigo_postal`
--

DROP TABLE IF EXISTS `codigo_postal`;
CREATE TABLE IF NOT EXISTS `codigo_postal` (
  `id_cp` int(11) NOT NULL AUTO_INCREMENT,
  `cp` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `id_colonia` int(11) NOT NULL,
  PRIMARY KEY (`id_cp`),
  KEY `id_colonia` (`id_colonia`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `codigo_postal`
--

INSERT INTO `codigo_postal` (`id_cp`, `cp`, `id_colonia`) VALUES
(1, '55765', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colonia`
--

DROP TABLE IF EXISTS `colonia`;
CREATE TABLE IF NOT EXISTS `colonia` (
  `id_colonia` int(11) NOT NULL AUTO_INCREMENT,
  `colonia` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `id_municipio` int(11) NOT NULL,
  PRIMARY KEY (`id_colonia`),
  KEY `id_municipio` (`id_municipio`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `colonia`
--

INSERT INTO `colonia` (`id_colonia`, `colonia`, `id_municipio`) VALUES
(1, 'Lomas de tecamac', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_venta`
--

DROP TABLE IF EXISTS `detalle_venta`;
CREATE TABLE IF NOT EXISTS `detalle_venta` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_produco` int(11) NOT NULL,
  `folio` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `precio_venta` decimal(9,0) NOT NULL,
  `cantidad` int(11) NOT NULL,
  PRIMARY KEY (`id_detalle`),
  KEY `id_produco` (`id_produco`),
  KEY `folio` (`folio`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `detalle_venta`
--

INSERT INTO `detalle_venta` (`id_detalle`, `id_produco`, `folio`, `fecha`, `precio_venta`, `cantidad`) VALUES
(1, 55, 1, '2018-03-22', '60', 3),
(2, 28, 1, '2018-03-23', '150', 3),
(3, 51, 2, '2018-03-24', '180', 1),
(4, 51, 3, '2018-03-24', '180', 2),
(5, 37, 4, '2018-03-25', '75', 1),
(6, 37, 5, '2018-03-26', '75', 1),
(7, 47, 5, '2018-03-26', '80', 1),
(8, 47, 6, '2018-03-26', '80', 1),
(9, 22, 7, '2018-03-26', '140', 1),
(10, 22, 8, '2018-03-26', '140', 2),
(11, 55, 10, '2018-03-26', '60', 1),
(12, 29, 11, '2018-03-26', '300', 1),
(13, 16, 12, '2018-03-27', '25', 1),
(14, 25, 13, '2018-03-27', '180', 1),
(15, 50, 14, '2018-03-27', '90', 1),
(16, 40, 14, '2018-03-27', '75', 1),
(17, 55, 15, '2018-03-28', '60', 1),
(18, 74, 15, '2018-03-28', '450', 1),
(19, 44, 16, '2018-03-31', '50', 1),
(20, 30, 16, '2018-03-31', '15', 1),
(21, 28, 16, '2018-04-01', '150', 1),
(22, 55, 16, '2018-04-01', '60', 1),
(23, 17, 16, '2018-04-02', '300', 1),
(24, 45, 17, '2018-04-03', '100', 1),
(25, 18, 17, '2018-04-04', '300', 1),
(26, 75, 17, '2017-12-12', '150', 1),
(27, 21, 18, '2018-03-29', '15', 1),
(28, 6, 19, '2018-03-29', '12', 1),
(29, 16, 20, '2018-03-29', '25', 1),
(30, 20, 20, '2018-03-29', '500', 1),
(31, 65, 20, '2018-03-29', '140', 1),
(32, 38, 20, '2018-03-29', '65', 1),
(33, 62, 21, '2018-03-29', '100', 1),
(34, 13, 21, '2018-03-29', '320', 1),
(35, 69, 21, '2018-03-29', '55', 1),
(36, 56, 22, '2018-04-09', '55', 2),
(37, 21, 22, '2018-04-09', '15', 1),
(38, 33, 22, '2018-04-09', '130', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

DROP TABLE IF EXISTS `estado`;
CREATE TABLE IF NOT EXISTS `estado` (
  `id_estado` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`id_estado`, `estado`) VALUES
(1, 'Mexico'),
(2, 'Hidalgo'),
(3, 'Pachuca'),
(4, 'Oaxaca'),
(5, 'Veracruz');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen`
--

DROP TABLE IF EXISTS `imagen`;
CREATE TABLE IF NOT EXISTS `imagen` (
  `id_imagen` int(11) NOT NULL AUTO_INCREMENT,
  `imagen` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_imagen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

DROP TABLE IF EXISTS `municipio`;
CREATE TABLE IF NOT EXISTS `municipio` (
  `id_municipio` int(11) NOT NULL AUTO_INCREMENT,
  `municipio` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `id_estado` int(11) NOT NULL,
  PRIMARY KEY (`id_municipio`),
  KEY `id_estado` (`id_estado`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `municipio`
--

INSERT INTO `municipio` (`id_municipio`, `municipio`, `id_estado`) VALUES
(1, 'Tecamac', 1),
(2, 'Tizayuca', 2),
(3, 'Tizayuca', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE IF NOT EXISTS `productos` (
  `id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `producto` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `precio_compra` decimal(9,2) NOT NULL,
  `precio_venta` decimal(9,2) NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  PRIMARY KEY (`id_producto`),
  KEY `id_imagen` (`imagen`),
  KEY `id_categoria` (`id_categoria`),
  KEY `id_proveedor` (`id_proveedor`),
  KEY `id_imagen_2` (`imagen`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `producto`, `precio_compra`, `precio_venta`, `descripcion`, `imagen`, `id_categoria`, `id_proveedor`) VALUES
(3, 'Hogar', '25.00', '120.00', 'Casa para aves', '63de13_a4.jpg', 6, 1),
(4, 'Limpiador de casas de aves', '58.00', '120.00', 'Spray', '64af20_a1.jpg', 6, 2),
(5, 'Cama de gatos', '100.00', '250.00', 'Cama especial para gatos color cafe', '80e57f_g10.jpg', 3, 1),
(6, 'Sobre de comida', '10.00', '12.00', 'Sobre de comida sabor res', 'b0b1a0_g11.jpg', 3, 1),
(7, 'Limpiador de peceras', '100.00', '150.00', 'Filtro de agua', '29ad67_8eb9b0_pez6.jpg', 5, 6),
(9, 'Bolsa de arena', '100.00', '120.00', 'Bolsa de arena para gatos 10k', 'd508a1_g8.jpg', 3, 1),
(10, 'Casa para peces', '300.00', '450.00', 'Casa color gris para pez ', '4f0286_r8.png', 5, 2),
(11, 'Arena para gatos', '200.00', '300.00', 'Arena para gato con perlas aromatizantes color morado', '787e40_g1.jpg', 3, 1),
(12, 'Alimentos', '200.00', '250.00', 'Alimentos para aves', '2711d5_a2.jpg', 6, 1),
(13, 'Bebederos', '300.00', '320.00', 'Bebederos para aves', 'b1b745_a3.jpg', 6, 2),
(14, 'Paquete de sobres de atun', '80.00', '100.00', 'Paquete con 4 sobres de comida sabor atun', 'fadaa7_g4.jpg', 3, 4),
(15, 'Cepillo ', '30.00', '50.00', 'Cepillo para gato con cerdas finas ', '2084da_g2.jpg', 3, 1),
(16, 'Leche ', '20.00', '25.00', 'Leche especial para gatos deslactosada', '8db800_g3.jpg', 3, 1),
(17, 'Repelente', '200.00', '300.00', 'Repelente para pulgas ', '27ade1_p2.jpg', 4, 2),
(18, 'Tapete', '200.00', '300.00', 'Tapete color azul', '76986d_p6.jpg', 4, 4),
(19, 'Croqueta rellena ', '100.00', '130.00', 'Croquetas wiskas  rellenas de atun ', 'bacbd5_g6.jpg', 3, 2),
(20, 'Cama ', '400.00', '500.00', 'Cama para perro color cafe ', '189f67_p4.jpg', 4, 1),
(21, 'hueso de carnaza ', '10.00', '15.00', 'Hueso color beige de carnaza', '4608ee_p5.jpg', 4, 2),
(22, 'galletas para perro', '100.00', '140.00', 'galletas para perro', 'a97f25_p12.jpg', 4, 4),
(23, 'alfombrilla para perro', '200.00', '213.00', 'alfombrilla para perro', '671b2c_p1.jpg', 4, 2),
(24, 'cojin ', '200.00', '250.00', 'cojin color cafe', '85011f_p7.jpg', 4, 1),
(25, 'collares de colores', '100.00', '180.00', 'collares de colores para perro', '3acced_p8.jpg', 4, 1),
(26, 'galleta limpiadora', '100.00', '130.00', 'Galleta limpiadora de dientes', 'd171dd_p7.jpg', 4, 1),
(27, 'plato ', '100.00', '130.00', 'plato naranja para perro', '502119_p10.jpg', 4, 1),
(28, 'shampoo', '100.00', '150.00', 'Shampoo dermoproctector', '69ded8_g5.jpg', 3, 1),
(29, 'croquetas 10k', '200.00', '300.00', 'croquetas ', '60241d_p11.jpg', 4, 2),
(30, 'Lata de alimento ', '10.00', '15.00', 'Lata de alimento, albondigas de buey con legumbres en salsa', '7f9d17_g7.jpg', 3, 2),
(31, 'Comida para hamster', '30.00', '50.00', 'Comida para animales pequeños', 'cde3ee_peque1.png', 8, 1),
(32, 'Jaula para roedores', '50.00', '75.00', 'Casa de animales pequeños', 'd65741_peque2.png', 8, 2),
(33, 'Tunel de Hamsters', '100.00', '130.00', 'Accesorios de hogar de animales pequeños', 'd0c78e_peque3.png', 8, 1),
(34, 'Hamsters y Gerbil', '110.00', '150.00', 'Comida energetica', '22eb18_peque4.png', 8, 1),
(35, 'Porta mascotas individual', '75.00', '100.00', 'Casa individual para hamsters', '034c0c_peque5.png', 8, 2),
(36, 'Naturatis', '65.00', '80.00', 'Vitaminas para hamster', '9ad967_peque6.png', 8, 1),
(37, 'Recogedor', '45.00', '75.00', 'Recogedor de desechos de Hamsters', '190089_peque7.png', 8, 1),
(38, 'Bebedero', '45.00', '65.00', 'Bebedero para animales pequeños', '8cb6be_peque8.png', 8, 2),
(39, 'Rueda de entrenamiento', '45.00', '70.00', 'Rueda para Hamsters', 'e27134_peque9.jpg', 8, 1),
(40, 'Bascula', '50.00', '75.00', 'Bascula de entrenamiento de Hamsters', 'c87c4f_peque10.png', 8, 1),
(41, 'Rueda giratoria', '30.00', '50.00', 'Rueda para jaula de animales pequeños', '190cda_peque11.jpg', 8, 1),
(42, 'Plato de juego', '45.00', '80.00', 'Plato para ejercicios de animales pequeños', 'c4e6c6_peque12.jpg', 8, 1),
(43, 'Casa de paja', '30.00', '45.00', 'Casa de paja para hogares de Hamsters', '88be40_peque13.jpg', 8, 1),
(44, 'Rueda de paja', '25.00', '50.00', 'Rueda de paja para ejercicio de Hamsters', '77c26b_peque14.jpg', 8, 2),
(45, 'Loción', '75.00', '100.00', 'Loción desparacitante de animales pequeños', 'e6337f_peque15.jpg', 8, 1),
(46, 'Alimentos digestivos', '120.00', '150.00', 'Alimentos digestivos para animales pequeños', 'f7130c_peque16.jpg', 8, 2),
(47, 'Cepillo', '50.00', '80.00', 'Acicalador de animales pequeños', '42264c_peque17.jpg', 8, 1),
(48, 'Compuestos', '45.00', '85.00', 'Compuestos nutricionales para animales pequeños', 'f8ab13_peque18.jpg', 8, 1),
(49, 'Alimento básico', '45.00', '75.00', 'Alimento básico para peces pequeños', '3c5af2_pez2.jpg', 5, 1),
(50, 'Alimentos compuestos', '60.00', '90.00', 'Alimentos para peces medios', '08c400_pez5.jpg', 5, 1),
(51, 'Tratamiento acuoso', '150.00', '180.00', 'Formula mineralizada para acuarios pequeños', '9191ed_pez3.jpg', 5, 1),
(52, 'Alimento comprimido', '90.00', '130.00', 'Alimento para peces grandes', '8cef47_pez4.JPG', 5, 4),
(53, 'Agua purificante', '75.00', '100.00', 'Agua neutralizante de parasitos acuosos', '61c00d_pez5.png', 5, 2),
(54, 'Pecera individual', '45.00', '70.00', 'Pecera pequeña de transporte', '6401b0_pez6.png', 5, 1),
(55, 'Red de pecera', '45.00', '60.00', 'Red para atrapar peces de pecera', 'b04795_pez7.png', 5, 1),
(56, 'Acondicionador acuoso', '35.00', '55.00', 'Sustancia para clarificar agua de peceras', '712254_pez8.png', 5, 2),
(57, 'Piedras de pecera', '45.00', '80.00', 'Piedras de decoración para peceras', 'ac396f_pez9.jpg', 5, 1),
(58, 'Sedimentos', '85.00', '100.00', 'Pequeños alimentos para peces', '502401_pez10.png', 5, 1),
(59, 'Pecera mediana ', '45.00', '95.00', 'Pecera de transporte mediano para peces', 'f40e47_pez11.jpg', 5, 1),
(60, 'Filtrador', '150.00', '200.00', 'Filtrador de agua para peceras', '6633d2_pez12.jpg', 5, 1),
(61, 'Luz blanca', '150.00', '200.00', 'Luz para peceras de intensidad baja', 'db55c1_pez13.png', 5, 1),
(62, 'Adorno para pecera', '75.00', '100.00', 'Adornos de flora para peceras', '2b36ec_pez15.jpg', 5, 1),
(63, 'Antiparasitos', '65.00', '100.00', 'Agente de limpieza para peceras', '197cfb_pez16.png', 5, 2),
(64, 'Filtro de oxigeno grander', '250.00', '350.00', 'Filtro de oxigeno para peceras grandes', '953a38_pez17.png', 5, 2),
(65, 'Suplementos alimenticios', '80.00', '140.00', 'Alimentos alternativos para peces', 'debc49_pez18.png', 5, 1),
(66, 'alimento tortuga', '21.00', '40.00', 'Alimento para cualquier tipo de tortuga', '51624b_images.jpg', 7, 4),
(67, 'Camarones para Tortuga', '40.00', '85.00', 'Alimento para cualquier tipo de tortuga', 'a76dea_r.jpg', 7, 4),
(68, 'calcium', '55.00', '95.00', 'Calcio para iguanas de cualquier edad', '793fb5_z.jpg', 7, 4),
(69, 'Iguana Sticks', '20.00', '55.00', 'Alimento para cualquier tipo de iguana', '8606c9_t.jpg', 7, 4),
(70, 'Suplemento de calcio', '45.00', '65.00', 'calcio para cualquier tipo de tortuga', 'a41f10_r5.png', 7, 4),
(71, 'Reptocal', '35.00', '75.00', 'Calcio para iguanas de cualquier edad', '505dd6_q.jpg', 7, 4),
(72, 'ibis', '60.00', '180.00', 'Alimento para aves insectivoras', 'd6eb7b_s.jpg', 6, 4),
(73, 'aufbau-kalk', '80.00', '140.00', 'Calcio para aves', '6df0cf_w.jpg', 6, 4),
(74, 'zakor', '250.00', '450.00', 'Vitaminas para gallos ', '70c41a_75dd6d_q.jpg', 6, 6),
(75, 'ornitol', '80.00', '150.00', 'Alimento para aves insectivoras', 'ea152c_j.jpg', 6, 5),
(76, 'Diclasuril', '250.00', '550.00', 'vitamina para gallina ', '2bf93f_72f5fb_f.jpg', 6, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
CREATE TABLE IF NOT EXISTS `proveedores` (
  `id_proveedor` int(11) NOT NULL AUTO_INCREMENT,
  `proveedor` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `contacto` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `tel` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `id_estado` int(11) NOT NULL,
  `id_municipio` int(11) NOT NULL,
  `id_colonia` int(11) NOT NULL,
  `id_cp` int(11) NOT NULL,
  `calle` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `no_ext` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `no_int` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_proveedor`),
  KEY `id_estado` (`id_estado`),
  KEY `id_municipio` (`id_municipio`),
  KEY `id_colonia` (`id_colonia`),
  KEY `id_cp` (`id_cp`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `proveedor`, `contacto`, `tel`, `correo`, `id_estado`, `id_municipio`, `id_colonia`, `id_cp`, `calle`, `no_ext`, `no_int`) VALUES
(1, 'Peddigre', 'Sebastian', '5555555555', 'sebas@gmail.com', 1, 1, 1, 1, 'Flor', '96', '86'),
(2, 'Purina', 'Octavio', '5555555555', 'octavio12@gmail.com', 1, 1, 1, 1, 'Loma Bonita', '15', '15'),
(4, 'vitacraft', 'vitacraft', '5506059752', 'vitacraft@hotmail.com', 1, 1, 1, 1, 'Protrero', '75', '43'),
(5, 'Trixie', 'Trixie', '5506059752', 'kronicko.966@hotmail.com', 1, 1, 1, 1, 'Estado de Tecámac', '9', '43'),
(6, 'Sglup', 'Pedro Zamora', '53230460', 'sglup@live.es', 1, 1, 1, 1, ' Avenida De Las Granjas', '321', '388');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `id_cliente` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `id_cliente` (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `password`, `estado`, `tipo`, `id_cliente`) VALUES
(1, '123456', 'Activo', 'Master', 1),
(2, '123456', 'Activo', 'Base', 5),
(3, '123456', 'Activo', 'Base', 6),
(4, '123456', 'Activo', 'Base', 7),
(5, '123456', 'Activo', 'Base', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

DROP TABLE IF EXISTS `venta`;
CREATE TABLE IF NOT EXISTS `venta` (
  `folio` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `monto` decimal(9,0) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `Fac_Rem` int(11) NOT NULL,
  PRIMARY KEY (`folio`),
  KEY `id_cliente` (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`folio`, `fecha`, `monto`, `id_cliente`, `Fac_Rem`) VALUES
(1, '2018-03-16', '630', 4, 1),
(2, '2018-03-16', '180', 4, 1),
(3, '2018-03-16', '360', 4, 1),
(4, '2018-03-17', '75', 4, 1),
(5, '2018-03-18', '155', 4, 1),
(6, '2018-03-18', '80', 4, 1),
(7, '2018-03-19', '140', 4, 1),
(8, '2018-03-19', '280', 4, 1),
(9, '2018-04-01', '0', 4, 1),
(10, '2018-04-01', '60', 7, 1),
(11, '2018-04-01', '300', 8, 1),
(12, '2018-04-02', '25', 8, 1),
(13, '2018-04-02', '180', 8, 1),
(14, '2018-04-02', '165', 8, 1),
(15, '2018-04-03', '510', 8, 1),
(16, '2018-04-03', '575', 8, 1),
(17, '2018-04-03', '550', 8, 1),
(18, '2018-04-04', '15', 6, 1),
(19, '2018-04-04', '12', 6, 1),
(20, '2018-04-04', '730', 6, 1),
(21, '2018-04-04', '475', 6, 1),
(22, '2018-04-09', '255', 6, 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`id_municipio`) REFERENCES `municipio` (`id_municipio`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `clientes_ibfk_2` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id_estado`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `clientes_ibfk_3` FOREIGN KEY (`id_colonia`) REFERENCES `colonia` (`id_colonia`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `clientes_ibfk_4` FOREIGN KEY (`id_cp`) REFERENCES `codigo_postal` (`id_cp`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `codigo_postal`
--
ALTER TABLE `codigo_postal`
  ADD CONSTRAINT `codigo_postal_ibfk_1` FOREIGN KEY (`id_colonia`) REFERENCES `colonia` (`id_colonia`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `colonia`
--
ALTER TABLE `colonia`
  ADD CONSTRAINT `colonia_ibfk_1` FOREIGN KEY (`id_municipio`) REFERENCES `municipio` (`id_municipio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD CONSTRAINT `detalle_venta_ibfk_1` FOREIGN KEY (`id_produco`) REFERENCES `productos` (`id_producto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detalle_venta_ibfk_2` FOREIGN KEY (`folio`) REFERENCES `venta` (`folio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD CONSTRAINT `municipio_ibfk_1` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id_estado`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`id_proveedor`) REFERENCES `proveedores` (`id_proveedor`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `productos_ibfk_2` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD CONSTRAINT `proveedores_ibfk_1` FOREIGN KEY (`id_cp`) REFERENCES `codigo_postal` (`id_cp`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `proveedores_ibfk_2` FOREIGN KEY (`id_colonia`) REFERENCES `colonia` (`id_colonia`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `proveedores_ibfk_3` FOREIGN KEY (`id_municipio`) REFERENCES `municipio` (`id_municipio`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `proveedores_ibfk_4` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id_estado`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
  ADD CONSTRAINT `venta_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
